﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using WBI.Model.DomainModel.TBL_ProjectTicket;

namespace Torch.Model.DomainModel.AutoMapper
{
    [Serializable()]
    public static class AuttoMapperSettings
    {
        /// <summary>
        /// Initializes mappings.
        /// </summary>
        public static void Initialize()
        {
           
            new TBL_ProjectTicketDomainModel().InitializeMapper();
            
        }
    }
}
