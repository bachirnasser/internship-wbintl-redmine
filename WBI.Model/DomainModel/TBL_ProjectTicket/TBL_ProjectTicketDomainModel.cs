﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Torch.Model.DomainModel;
using WBI.DataLayer.MainData;
using WBI.Model.ViewModel;


namespace WBI.Model.DomainModel.TBL_ProjectTicket
{
   public class TBL_ProjectTicketDomainModel 
    {





        public string[] SaveProjectTicket(TBL_ProjectTicketViewModel etProjectTicketViewModel, bool isAPI = false)
        {
            string[] messages = new string[2];
            messages[1] = SaveBuisnessLogic(etProjectTicketViewModel);
            if (messages[1] != string.Empty)
            {
                messages[0] = "error";
                return messages;
            }


            WBI.DataLayer.MainData.TBL_ProjectTicket etDamage = new WBI.DataLayer.MainData.TBL_ProjectTicket();
        

            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    using (var db = new RedmineEntities())
                    {

                        if (etProjectTicketViewModel.TBL_ProjectTicket_ID != 0 && etProjectTicketViewModel.TBL_ProjectTicket_ID.ToString() != "")
                        {   

                            var etDamageGurantordb = db.TBL_ProjectTicket.Where(p => p.TBL_ProjectTicket_ID.ToString() == etProjectTicketViewModel.TBL_ProjectTicket_ID.ToString()).FirstOrDefault();

                            etDamage = Mapper.Map<TBL_ProjectTicketViewModel, WBI.DataLayer.MainData.TBL_ProjectTicket>(etProjectTicketViewModel);

                            if (etDamageGurantordb != null)
                            {
                               
                                string[] properties = new string[8];

                                int count = -1;


                                if (etProjectTicketViewModel.TBL_ProjectID != null)
                                {
                                    count++;
                                    properties[count] = "TBL_ProjectID";
                                }




                                if (etProjectTicketViewModel.TBL_Project_Description != null)
                                {
                                    count++;
                                    properties[count] = "TBL_Project_Description";
                                }
                                if (etProjectTicketViewModel.TBL_ProjectTicket_date != null)
                                {
                                    count++;
                                    properties[count] = "TBL_ProjectTicket_date";
                                }
                                if (etProjectTicketViewModel.TBL_ProjectTicket_isActibe != null)
                                {
                                    count++;
                                    properties[count] = "TBL_ProjectTicket_isActibe";
                                }
                              
                                Torch.UtilityComponent.Utilities.MergeObject(etDamageGurantordb, etProjectTicketViewModel, true,
                                      properties
                                     );
                               

                             
                                
                                db.SaveChanges();
                            }
                        }
                        else
                        {

                            etDamage = Mapper.Map<TBL_ProjectTicketViewModel, WBI.DataLayer.MainData.TBL_ProjectTicket>(etProjectTicketViewModel);



                            db.TBL_ProjectTicket.Add(etDamage);
                            db.SaveChanges();





                        }

                        messages[0] = "s";
                        messages[1] = etDamage.TBL_ProjectTicket_ID.ToString();
                        //   savedSuccessfully = true;
                        scope.Complete();

                    }
                }
                catch (Exception ex)
                {
                    messages[0] = "e";
                    messages[1] = ex.InnerException.InnerException.Message;
                    //  message = ex.Message;
                    return messages;
                    //   savedSuccessfully = false;
                    //return false;
                }
            }
            return messages;

        }


        public string SaveBuisnessLogic(TBL_ProjectTicketViewModel model)
        {
            string message = string.Empty;
            return message;
        }






        #region Private Methods
        public void InitializeMapper()

        {
            #region Database To View

            Mapper.CreateMap<WBI.DataLayer.MainData.TBL_ProjectTicket, TBL_ProjectTicketViewModel>()
                  .IgnoreAllNonExisting();

            //Mapper.CreateMap<USP_Get_DamageCategory_Result, DamageCategoryGridViewModel>()
            //      .IgnoreAllNonExisting();

         #endregion

            #region view to database

            Mapper.CreateMap<TBL_ProjectTicketViewModel, WBI.DataLayer.MainData.TBL_ProjectTicket>()
                  .IgnoreAllNonExisting();

            ////Mapper.CreateMap<DamageCategoryGridViewModel, USP_Get_DamageCategory_Result>()
            ////    .IgnoreAllNonExisting();

           #endregion


        }
        #endregion











    }
}
