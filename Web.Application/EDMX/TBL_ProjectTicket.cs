//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Web.Application.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class TBL_ProjectTicket
    {
        public int TBL_ProjectTicket_ID { get; set; }
        public Nullable<int> TBL_ProjectID { get; set; }
        public string TBL_Project_Description { get; set; }
        public string TBL_ProjectTicket_date { get; set; }
        public Nullable<bool> TBL_ProjectTicket_isActive { get; set; }
        public string TBL_ProjectName { get; set; }
    }
}
