﻿using Newtonsoft.Json;
using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WBI.Model.DomainModel.TBL_ProjectTicket;
using WBI.Model.ViewModel;
using Web.Application.EDMX;

namespace Web.Application.Controllers
{
    public class CountdownController : Controller
    {
        // GET: Countdown


        public static List<Dictionary<string, string>> GlobalIssues;

        public ActionResult Index()
        {

           


            return View();
        }




        public JsonResult getAllProject() {


            Dictionary<string, List<Dictionary<string, string>>> issuesDictionary = new Dictionary<string, List<Dictionary<string, string>>>();

            List<Dictionary<string, string>> ListProject = new List<Dictionary<string, string>>();
            List<Dictionary<string, string>> ListDictionary = new List<Dictionary<string, string>>();



            try
            {

                string username = "anthony.najour";
                string password = "123456";

                var url = FirstWebApp.Globals.Configuration.REDMINE;

                var manager = new RedmineManager(url, username, password);
                var user = manager.GetCurrentUser();

                var parameters = new NameValueCollection { { "status_id", "*" } };

                var table = manager.GetObjects<Project>(parameters);


                foreach (var p in table)
                {
                    try
                    {

                        Dictionary<string, string> DictionaryProject = new Dictionary<string, string>();

                        DictionaryProject.Add("ProjectId", p.Id == -1 ? "" : "" + p.Id);


                        DictionaryProject.Add("ProjectName", p.Name == null ? "" : p.Name);
                       









                        ListDictionary.Add(DictionaryProject);
                    }
                    catch (Exception ex)
                    {

                    }

                }


                GlobalIssues = ListDictionary;


          ListProject = GlobalIssues.GroupBy(x => x["ProjectId"])
                             .Select(grp => grp.First())
                              .ToList();










            }
            catch { }


            issuesDictionary.Add("data", ListProject);



            issuesDictionary.Reverse();

            var jsonResult = JsonConvert.SerializeObject(issuesDictionary);


            return Json(jsonResult, JsonRequestBehavior.AllowGet);



        }





        [HttpPost]
        public string AddProjectTicket(int projectTicketid, int projectid, string projectname, string desc , string date)
        {
            try
            {
                using (
                    RedmineEntities RedmineEntity = new RedmineEntities())
                {
                    TBL_ProjectTicket ProjectTicket = new TBL_ProjectTicket();

                    ProjectTicket.TBL_ProjectTicket_ID = projectTicketid;
                    ProjectTicket.TBL_ProjectID = projectid;
                    ProjectTicket.TBL_ProjectName = projectname;
                    ProjectTicket.TBL_Project_Description = desc;
                    ProjectTicket.TBL_ProjectTicket_date = date;
                    ProjectTicket.TBL_ProjectTicket_isActive = true;


                    RedmineEntity.TBL_ProjectTicket.Add(ProjectTicket);

                    RedmineEntity.SaveChanges();

                    return "Success";
                }
            }
            catch
            {
                return "An Error has occured";
            }

        }


        [HttpPost]
        public JsonResult GetProjectTicket()
        {
            Dictionary<string, List<TBL_ProjectTicket>> Dictionary = new Dictionary<string, List<TBL_ProjectTicket>>();

            TBL_ProjectTicket ProjectsAvailable = new TBL_ProjectTicket();
            List<TBL_ProjectTicket> ListProjectsAvailable = new List<TBL_ProjectTicket>();

            using (RedmineEntities RedmineEntity = new RedmineEntities())
            {
                ListProjectsAvailable = RedmineEntity.TBL_ProjectTicket.ToList();
            }




            Dictionary.Add("data", ListProjectsAvailable);



            var jsonResult = JsonConvert.SerializeObject(Dictionary);




            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }









    }
}