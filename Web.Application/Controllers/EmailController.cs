﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



//using MVCEmail.Models;
using System.Threading.Tasks;
using Web.Application.Models;
using System.Net;
using System.Net.Mail;
using System.IO;

namespace Web.Application.Controllers
{
    public class EmailController : Controller
    {
        // GET: Email
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailFormModel model)
        {
            if (ModelState.IsValid)
            {
                var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                var message = new MailMessage();

                message.To.Add(new MailAddress("51410009@students.liu.edu.lb"));  // replace with valid value 
                message.Bcc.Add(new MailAddress("51410009@students.liu.edu.lb"));


                message.From = new MailAddress("51410009@students.liu.edu.lb");  // replace with valid value
                message.Subject = "Your email subject";
                message.Body = string.Format(body, model.FromName, model.FromEmail, model.Message);
                message.IsBodyHtml = true;

                if (model.Upload != null && model.Upload.ContentLength > 0)
                {
                    message.Attachments.Add(new Attachment(model.Upload.InputStream, Path.GetFileName(model.Upload.FileName)));
                }



                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "51410009@students.liu.edu.lb",  // replace with valid value
                        Password = "Iphone4s"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    // smtp.Host = "smtp-mail.outlook.com"; //for outlook and live
                    smtp.Host = "smtp.gmail.com";// for gmail

                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            return View(model);
        }

        public ActionResult Sent()
        {
            return View();
        }


    }
}