﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;


namespace Web.Application.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
     

        public ActionResult InfoUsers()
        {
            return View();
        }






        public JsonResult GetProjects()
        {
            string username = "anthony.najour";
            string password = "123456";
            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);

            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };

            var AllData = manager.GetObjects<Issue>(parameters).ToList();

            List<string> Projects = new List<string>();

            foreach (var Issue in AllData)
            {
                try
                {
                    Projects.Add(Issue.Project.Name);
                }
                catch (Exception ex) { }
            }


            List<string> Projectsdistinct = Projects.Distinct().ToList();
            var jsonResultProjects = JsonConvert.SerializeObject(Projectsdistinct);
            return Json(jsonResultProjects, JsonRequestBehavior.AllowGet);

        }






        public JsonResult getTicketUsers(string Name )
        {
            string username = "anthony.najour";
            string password = "123456";

            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);

            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };

            var AllData = manager.GetObjects<Issue>(parameters).ToList();

            List<string> UserProjects1 = new List<string>();
            // List<string> UserProjects2 = new List<string>();

            List<string> users = new List<string>();

            foreach (var Issue in AllData)
            {
                try
                {
                    //if (Issue.AssignedTo.Name == "Jihad Fares")
                    //{
                    //    UserProjects1.Add(Issue.AssignedTo.Name + ',' + Issue.Project.Id);
                    //}
                    if (Issue.Project.Name == Name)
                    {
                        // UserProjects1.Add(Issue.Project.Id.ToString());
                        UserProjects1.Add(Issue.AssignedTo.Name );
                    }
                }


                catch (Exception ex) { }
            }

            string UserProjects = string.Join(",", UserProjects1);

            var NbrUserProjectsB = UserProjects1.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => new { Element = y.Key, Counter = y.Count() })
              .ToList();
        

            var jsonNbrUserProjectsB = JsonConvert.SerializeObject(NbrUserProjectsB);


            return Json(jsonNbrUserProjectsB, JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetTableTickets(string objectA)
        {
            string username = "anthony.najour";
            string password = "123456";

            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);

            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };

            var AllData = manager.GetObjects<Issue>(parameters).ToList();

            List<Dictionary<string,string>> INfoTicketsUsers = new List<Dictionary<string,string>>();

             



        var test = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(objectA);

            foreach (var item in test)
            {

                string name = item["Name"].ToString();
                string project = item["project"].ToString();
                foreach (var Issue in AllData)
                { int i= +0;
                    try
                    {
                        
                        if (Issue.AssignedTo.Name == name & Issue.Project.Name == project)
                        {
                            // INfoTicketsUsers.Add(Issue.Id + Issue.AssignedTo.Name + Issue.Author.Name);
                            Dictionary<string, string> MyList = new Dictionary<string, string>();

                            MyList.Add("ID", Issue.Id.ToString());
                            MyList.Add("AssignedTo", Issue.AssignedTo.Name);
                            MyList.Add("Status", Issue.Status.ToString());
                            MyList.Add("Project", Issue.Project.Name);
                            MyList.Add("Author", Issue.Author.Name);


                            INfoTicketsUsers.Add(MyList);
                        }

                    }
                    
                    catch (Exception ex) { }
                }

            }
          
            //string INfoTicketsUsersS = string.Join(",", INfoTicketsUsers);

            //var NbrINfoTicketsUsers = INfoTicketsUsers.GroupBy(x => x)
            //  .Where(g => g.Count() > 1)
            //  .Select(y => new { Element = y.Key, Counter = y.Count() })
            //  .ToList();
            
            var jsonINfoTicketsUsers = JsonConvert.SerializeObject(INfoTicketsUsers);
            
            return Json(INfoTicketsUsers, JsonRequestBehavior.AllowGet);

        }
    }
} 