﻿using Newtonsoft.Json;
using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Application.Models;
using Web.Application.Session;



namespace Web.Application.Controllers
{
    public class TicketsController : Controller
    {
 
        public ActionResult Index()
        {
           return View();
                }
        public ActionResult InfoTickets()
        {
            return View();
        }

        public JsonResult getAllUsers1(string projectName, string format)
        {
         string username = "anthony.najour";
            string password = "123456";
            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);

            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };

            var AllData = manager.GetObjects<Issue>(parameters).ToList();

           List<string> users = new List<string>();

          foreach (var Issue in AllData)
            {
                try
                {
                   users.Add(Issue.AssignedTo.Name);
                    }
                       catch (Exception ex) { }
                                          }
              List<string> usersdistinct = users.Distinct().ToList();
                var jsonResultEmpls = JsonConvert.SerializeObject(usersdistinct);
               return Json(jsonResultEmpls, JsonRequestBehavior.AllowGet);
          
            }









        public JsonResult getTicketByUser (string Name)
        {
            string username = "anthony.najour";
            string password = "123456";

            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);

            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };

            var AllData = manager.GetObjects<Issue>(parameters).ToList();

           List<string> UserProjects1 = new List<string>();
           // List<string> UserProjects2 = new List<string>();
 
            List<string> users = new List<string>();

            foreach (var Issue in AllData)
            {
                try
                {
                    //if (Issue.AssignedTo.Name == "Jihad Fares")
                    //{
                    //    UserProjects1.Add(Issue.AssignedTo.Name + ',' + Issue.Project.Id);
                    //}
                    if (Issue.AssignedTo.Name == Name)
                    {
                        UserProjects1.Add(Issue.Project.Id.ToString());
                    
   }             }


                catch (Exception ex) { }
                        }

            string UserProjects = string.Join(",", UserProjects1);
           // string UserProjects22 = string.Join(",", UserProjects2);

            var NbrUserProjectsB = UserProjects1.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => new { Element = y.Key, Counter = y.Count() })
              .ToList();
            ////var NbrUserProjectsB = UserProjects2.GroupBy(x => x)
            ////  .Where(g => g.Count() > 1)
            ////  .Select(y => new { Element = y.Key, Counter = y.Count() })
            ////  .ToList();
            // int distinctCount = UserProjects1.Distinct().Count();
            //List<string> usersdistinct = users.Distinct().ToList();

            //var jsonResultEmpls = JsonConvert.SerializeObject(usersdistinct);

            var jsonNbrUserProjectsB = JsonConvert.SerializeObject(NbrUserProjectsB);


            //var result = new { jsonResultEmpls, jsonNbrUserProjectsB };
            return Json(jsonNbrUserProjectsB, JsonRequestBehavior.AllowGet);

    }








        public JsonResult getInfTicketsByUser(string Name, int Id)
        {
            string username = "anthony.najour";
            string password = "123456";

            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);

            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };

            var AllData = manager.GetObjects<Issue>(parameters).ToList();

            //List<string> InfTicketsId = new List<string>();
            List<Dictionary<string, string>> InfTicketsId = new List<Dictionary<string, string>>();
            // List<string> UserProjects2 = new List<string>();

            List<string> users = new List<string>();


            foreach (var Issue in AllData)
            {
                try
                {
                    
                    if (Issue.AssignedTo.Name == Name & Issue.Project.Id == Id)
                    {

                        //InfTicketsId.Add(Issue.Id + Issue.Project.Id+Issue.Author.Name+Issue.Category+Issue.Priority.Name+Issue.StartDate+Issue.Status+Issue.Subject);
                        Dictionary<string, string> MyList1 = new Dictionary<string, string>();

                        MyList1.Add("ID", Issue.Id.ToString());
                        MyList1.Add("AssignedTo", Issue.AssignedTo.Name);
                        MyList1.Add("Status", Issue.Status.ToString());
                        MyList1.Add("Project", Issue.Project.Name);
                        MyList1.Add("Author", Issue.Author.Name);


                        InfTicketsId.Add(MyList1);
                    }
                }


                catch (Exception ex) { }
            }

            //string UserProjects = string.Join(",", InfTicketsId);
            

            //var jsonInfTicketsId = JsonConvert.SerializeObject(InfTicketsId);

            return Json(InfTicketsId, JsonRequestBehavior.AllowGet);

        }











    } }


  

    
    