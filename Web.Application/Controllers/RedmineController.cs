﻿using Newtonsoft.Json;
using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Application.Models;
using Web.Application.Session;

namespace Web.Application.Controllers
{
    [Authorize]

  

    public class RedmineController : Controller
    {
        // GET: RedmineController
     public static  List<Dictionary<string, string>> GlobalIssues;


      

        public ActionResult Index()
        {
       
            GlobalIssues = getAllissues();

          var alltickets = GlobalIssues.Count();

            var newtickets = GlobalIssues.Where(x => x["Status"] == "New").Count();


            var nbOfProject = GlobalIssues.GroupBy(x => x["ProjectId"])
                  .Select(grp => grp.First())
              .ToList().Count();


            var nbOfEmployee = GlobalIssues.GroupBy(x => x["AssignedTo"])
                 .Select(grp => grp.First())
             .ToList().Count();




            ViewBag.alltickets = alltickets;
           ViewBag.newtickets = newtickets;  
            ViewBag.nbOfProject = nbOfProject;
             ViewBag.nbOfEmployee = nbOfEmployee;

            
            string value = Session.GetDataFromSession<string>("Email");
            ViewData["sessionStringByExtensions"] = value;

            return View();
        }



        public JsonResult getAllUsers(string projectName, string format)
  {

            Dictionary<string, List<Models.Project>> issuesDictionary = new Dictionary<string, List<Models.Project>>();

            Dictionary<string, List<Models.WorkOnProject>> WorkonDictionary = new Dictionary<string, List<Models.WorkOnProject>>();




            List<object> o = new List<object>();

            Dictionary<string, List<object>> Dictionary = new Dictionary<string, List<object>>();

            List<Models.Project> Listproject = new List<Models.Project>();
            List<Models.WorkOnProject> ListWorkOnProject = new List<Models.WorkOnProject>();









            var f1 = GlobalIssues.GroupBy(x => x["ProjectId"])
                           .Select(grp => grp.First())
                           .Select(t => new[] { t["ProjectId"], t["ProjectName"] })
                            .ToList();




            foreach (var t in f1)
            {
                var people = GlobalIssues.Where(x => x["ProjectId"] == t[0].ToString()).Select(v => new[] { v["AssignedToId"] })
                            .Distinct().ToList();

                var people1 = GlobalIssues.Where(x => x["ProjectId"] == t[0].ToString()).Select(v => new[] { v["ProjectId"], v["ProjectName"], v["AssignedToId"], v["AssignedTo"] })
                           .Distinct().ToList();



              var EmployeePerProject = people.SelectMany(grp => grp)
                         .Distinct()
                          .ToList().Count();

  var people3 = GlobalIssues.Where(x => x["ProjectId"] == t[0].ToString()).Select(v => new[] { v["AssignedToId"] })
                         .SelectMany(grp => grp)
                         .Distinct()
                          .ToList().Count();


                Listproject.Add(new Models.Project(int.Parse(t[0]), t[1], EmployeePerProject));



            }




            foreach (var t in GlobalIssues)
            {
                Dictionary<string, string> issuesList = new Dictionary<string, string>();

                try
                {
                    
                int ProjectId = int.Parse( t["ProjectId"]);

                var ProjectName = t["ProjectName"];

                int AssignedToId = int.Parse(t["AssignedToId"]);

                var AssignedTo = t["AssignedTo"];


               

                    Models.WorkOnProject WorkOnProject = new Models.WorkOnProject(ProjectId, ProjectName, AssignedToId, AssignedTo, 1);



                    //   var containsItem = Listproject.Any(item => item.projectId == project.projectId);

                    if (!ListWorkOnProject.Any(item => item.projectId == WorkOnProject.projectId && item.workerId == WorkOnProject.workerId))
                    {//if does not exist


                        //  Listproject.First(d => d.projectId == project.projectId).NumberOfWorker++;

                        ListWorkOnProject.Add(WorkOnProject);

                    }

                    else
                    {
                        ListWorkOnProject.First(d => d.projectId == WorkOnProject.projectId && d.workerId == WorkOnProject.workerId).stillHave++;

                    }



                   


                }
                catch (Exception ex)
                {

                }




            }




            Listproject.Sort((x, y) => string.Compare("" + x.projectId, "" + y.projectId));

            ListWorkOnProject.Sort((x, y) => string.Compare(""+x.projectId, ""+y.projectId));




            //to sort the same list

            issuesDictionary.Add("dataproject", Listproject);

            WorkonDictionary.Add("dataUser", ListWorkOnProject);


          //  Dictionary.Add("data", Listproject);

            o.Add(issuesDictionary);
            o.Add(WorkonDictionary);

            Dictionary.Add("data",o);

            Dictionary.Reverse();

            var jsonResult = JsonConvert.SerializeObject(Dictionary);



            return Json(jsonResult, JsonRequestBehavior.AllowGet);



        }


      
        
        public JsonResult gettoPie(string projectName, string format)
        {
            List<int> PendingTasks = new List<int>();
            PendingTasks.Add(1);
            PendingTasks.Add(2);
            PendingTasks.Add(4);

            Dictionary<string, Dictionary<string, Dictionary<string, string>>> issuesDictionaryLast = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();



            Dictionary<string, Dictionary<string, string>> issuesDictionary = new Dictionary<string, Dictionary<string, string>>();

            Dictionary<string, string> issuesList1 = new Dictionary<string, string>();
            Dictionary<string, string> issuesList2 = new Dictionary<string, string>();
            Dictionary<string, string> issuesList3 = new Dictionary<string, string>();

            string username = "anthony.najour";
            string password = "123456";

            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);
            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };

            //   int NumberOfIssues = manager.GetObjects<Issue>(parameters).Count();

            double NumberOfAnthonyNew = 0;
            double NumberOfTraineeNew = 0;
            double NumberOfOthersNew = 0;

            var AnthonyName = "";
            var TraineeName = "";
            var OtherName = "Others";

            double numberForAllNew = 0;

            foreach (var issue in manager.GetObjects<Issue>(parameters))
            {
                if (PendingTasks.Contains(issue.Status.Id))
                {
                    if (issue.Status.Name == "New")
                    {

                        numberForAllNew++;

                        if (issue.AssignedTo.Id == 89)
                        {//Anthony

                            NumberOfAnthonyNew++;
                            AnthonyName = issue.AssignedTo.Name;
                        }

                        else if (issue.AssignedTo.Id == 90)//trainee
                        {
                            TraineeName = issue.AssignedTo.Name;

                            NumberOfTraineeNew++;
                        }
                        else
                        {//others

                            NumberOfOthersNew++;
                        }



                        var c = issue.AssignedTo.Id;

                        var c1 = issue.AssignedTo.Name;




                    }




                }
            }

            double Anthonypercentage = (NumberOfAnthonyNew * 100) / numberForAllNew;

            double Traineepercentage = (NumberOfTraineeNew * 100) / numberForAllNew;

            double otherpercentage = (NumberOfOthersNew * 100) / numberForAllNew;


            issuesList1.Add("name", AnthonyName);
            issuesList1.Add("ticket", "" + Anthonypercentage);

            issuesList2.Add("name", TraineeName);
            issuesList2.Add("ticket", "" + Traineepercentage);

            issuesList3.Add("name", OtherName);
            issuesList3.Add("ticket", "" + otherpercentage);


            issuesDictionary.Add("e1", issuesList1);
            issuesDictionary.Add("e2", issuesList2);
            issuesDictionary.Add("e3", issuesList3);




            issuesDictionaryLast.Add("employee", issuesDictionary);



            issuesDictionary.Reverse();

            var jsonResult = JsonConvert.SerializeObject(issuesDictionary);


            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getToBarChart(string name, string projectName, string format)
        {

            List<int> PendingTasks = new List<int>();
            PendingTasks.Add(1);
            PendingTasks.Add(2);
            PendingTasks.Add(4);

            Dictionary<string, Dictionary<string, Dictionary<string, string>>> issuesDictionaryLast = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();



            Dictionary<string, Dictionary<string, string>> issuesDictionary = new Dictionary<string, Dictionary<string, string>>();

            Dictionary<string, string> issuesList1 = new Dictionary<string, string>();
            Dictionary<string, string> issuesList2 = new Dictionary<string, string>();
            Dictionary<string, string> issuesList3 = new Dictionary<string, string>();

            string username = "anthony.najour";
            string password = "123456";

            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);
            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };

            //   int NumberOfIssues = manager.GetObjects<Issue>(parameters).Count();


            double NumberOfAnthonyNew = 0;
            double NumberOfTraineeNew = 0;
            double NumberOfOthersNew = 0;

            var AnthonyName = "";
            var TraineeName = "";
            var OtherName = "Others";

            double numberForAllNew = 0;

            foreach (var issue in manager.GetObjects<Issue>(parameters))
            {
                if (PendingTasks.Contains(issue.Status.Id))
                {
                    if (issue.Status.Name == "New")
                    {

                        numberForAllNew++;

                        if (issue.AssignedTo.Id == 89)
                        {//Anthony

                            NumberOfAnthonyNew++;
                            AnthonyName = issue.AssignedTo.Name;
                        }

                        else if (issue.AssignedTo.Id == 90)//trainee
                        {
                            TraineeName = issue.AssignedTo.Name;

                            NumberOfTraineeNew++;
                        }
                        else
                        {//others

                            NumberOfOthersNew++;
                        }



                        var c = issue.AssignedTo.Id;

                        var c1 = issue.AssignedTo.Name;




                    }




                }
            }

            double Anthonypercentage = (NumberOfAnthonyNew * 8) / numberForAllNew;

            double Traineepercentage = (NumberOfTraineeNew * 8) / numberForAllNew;

            double otherpercentage = (NumberOfOthersNew * 8) / numberForAllNew;





            issuesList1.Add("name", AnthonyName);
            issuesList1.Add("ticket", "" + Anthonypercentage);

            issuesList2.Add("name", TraineeName);
            issuesList2.Add("ticket", "" + Traineepercentage);

            issuesList3.Add("name", OtherName);
            issuesList3.Add("ticket", "" + otherpercentage);


            issuesDictionary.Add("e1", issuesList1);
            issuesDictionary.Add("e2", issuesList2);
            issuesDictionary.Add("e3", issuesList3);




            issuesDictionaryLast.Add("employee", issuesDictionary);



            issuesDictionary.Reverse();

            var jsonResult = JsonConvert.SerializeObject(issuesDictionary);


            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getAllProject(string projectName, string format)
        {



            Dictionary<string, List<Dictionary<string, string>>> issuesDictionary = new Dictionary<string, List<Dictionary<string, string>>>();

            

            List<Dictionary<string, string>> ListProject = GlobalIssues.GroupBy(x => x["ProjectId"])
                               .Select(grp => grp.First())
                                .ToList();


            


            issuesDictionary.Add("data", ListProject);



            issuesDictionary.Reverse();

            var jsonResult = JsonConvert.SerializeObject(issuesDictionary);


            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }





        public JsonResult RedminePendingTickets(int projectId, string projectName, string format)

        {
            List<int> PendingTasks = new List<int>();
            PendingTasks.Add(1);
            PendingTasks.Add(2);
            PendingTasks.Add(4);




            

            Dictionary<string, List<Dictionary<string, string>>> issuesDictionary = new Dictionary<string, List<Dictionary<string, string>>>();





            List<Dictionary<string, string>> ListRedminePendingTickets = GlobalIssues.Where(x => (x["StatusId"] == "1" || x["StatusId"] == "2" || x["StatusId"] == "4")&&x["ProjectId"]== projectId.ToString()).ToList();



            issuesDictionary.Add("data", ListRedminePendingTickets);




            issuesDictionary.Reverse();

            var jsonResult = JsonConvert.SerializeObject(issuesDictionary);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }


        public JsonResult RedmineClosedTickets(int projectId, string projectName, string format)
        {
            List<int> ClosedTasks = new List<int>();
            ClosedTasks.Add(3);
            ClosedTasks.Add(5);


            Dictionary<string, List<Dictionary<string, string>>> issuesDictionary = new Dictionary<string, List<Dictionary<string, string>>>();





            List<Dictionary<string, string>> ListRedminePendingTickets = GlobalIssues.Where(x => ( x["StatusId"] == "3" || x["StatusId"] == "5") && x["ProjectId"] == projectId.ToString()).ToList();



            issuesDictionary.Add("data", ListRedminePendingTickets);




            issuesDictionary.Reverse();

            var jsonResult = JsonConvert.SerializeObject(issuesDictionary);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        


        public JsonResult RedmineNewTicket(string taskSubject, int taskTracker, int taskPriority, int taskProject, string taskNote = null)
        {
            try
            {
                Dictionary<int, Dictionary<string, string>> issuesDictionary = new Dictionary<int, Dictionary<string, string>>();

      


                string username = "anthony.najour";
                string password = "123456";

                var url = FirstWebApp.Globals.Configuration.REDMINE;

                var manager = new RedmineManager(url, username, password);

                var newIssue = new Issue
                {
                    Subject = taskSubject,
                    AssignedTo = new IdentifiableName { Id = 47 }, //Username: Bachir - Id: 47
                    Tracker = new IdentifiableName { Id = taskTracker },
                    Priority = new IdentifiableName { Id = taskPriority },
                    Notes = taskNote,
                    Project = new IdentifiableName { Id = taskProject }
                };

                manager.CreateObject(newIssue);
              //  var result = new { Success = "True", Message = "Success Message" };

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
              //  var result = new { Success = "False", Message = "Error Message" };

                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }


      
        public List<Dictionary<string, string>> getAllissues()


        {

            List<Dictionary<string, string>> ListDictionary = new List<Dictionary<string, string>>();



            try
            {

                string username = "anthony.najour";
                string password = "123456";

                var url = FirstWebApp.Globals.Configuration.REDMINE;

                var manager = new RedmineManager(url, username, password);
                var user = manager.GetCurrentUser();

                var parameters = new NameValueCollection { { "status_id", "*" } };

                var table= manager.GetObjects<Issue>(parameters);


            foreach (var issue in table)
            {
                try
                {

                    Dictionary<string, string> issuesList = new Dictionary<string, string>();

                    issuesList.Add("issueId", issue.Id == -1 ? "" : "" + issue.Id);


                        issuesList.Add("Author", issue.Author == null ? "" : issue.Author.Name);
                    issuesList.Add("AssignedToId", issue.AssignedTo.Id.ToString() == null ? "" : "" + issue.AssignedTo.Id);
                    issuesList.Add("AssignedTo", issue.AssignedTo == null ? "" : issue.AssignedTo.Name);

                      
                    issuesList.Add("StatusId", issue.Status.Id == -1 ? "" : "" + issue.Status.Id);
                    issuesList.Add("Status", issue.Status == null ? "" : issue.Status.Name);
                    issuesList.Add("Subject", issue.Subject);
                    issuesList.Add("Description", issue.Description);

                        issuesList.Add("StartDate", issue.StartDate == null ? "" : issue.StartDate.ToString());
                        issuesList.Add("CreatedOn", issue.CreatedOn == null ? "" : issue.CreatedOn.ToString());
                    issuesList.Add("UpdatedOn", issue.UpdatedOn == null ? "" :  issue.UpdatedOn.ToString());

                        issuesList.Add("Priority", issue.Priority == null ? "" : issue.Priority.Name);

                    issuesList.Add("ProjectId", issue.Project.Id == -1 ? "" : "" + issue.Project.Id);
                    issuesList.Add("ProjectName", issue.Project.Name == null ? "" : issue.Project.Name);


                       
                     





                        ListDictionary.Add(issuesList);
                }
                catch (Exception ex)
                {

                }

            }


            GlobalIssues = ListDictionary;



            }
            catch { }

            return ListDictionary;

          

        }



        
        public JsonResult getAllProjectPerTicket(string projectName, string format)
        {

           
         

            Dictionary<string, List<Dictionary<string, string>>> Dictionary = new Dictionary<string, List<Dictionary<string, string>>>();

         


            List<Dictionary<string, string>> ListOfEvents = new List<Dictionary<string, string>>();
          


            var ListProject = GlobalIssues.GroupBy(x => x["ProjectId"])
                               .Select(grp => grp.First())
                               .Select(t => new[] { t["ProjectId"] })
                            .ToList();



            var ListOfDate = GlobalIssues.GroupBy(x => x["StartDate"])
                           .Select(grp => grp.First())
                           .Select(t => new[] { t["StartDate"] })
                            .ToList();

            List<Dictionary<string, string>> ListOfcolors = new List<Dictionary<string, string>>();



            List<string> listhexColor = new List<string>();

            listhexColor.Add("#000000");
            listhexColor.Add("#ec0000");
            listhexColor.Add("#007500");
            listhexColor.Add("#88acab");
            listhexColor.Add("#9b357b");
            listhexColor.Add("#ffcc00");
            listhexColor.Add("#b76e79");
            listhexColor.Add("#5e0303");
            listhexColor.Add("#ff1493");
            listhexColor.Add("#00FFFF");




            int count = 0;

            foreach (var p in ListProject)
            {
                Dictionary<string, string> Dictionarycolor = new Dictionary<string, string>();

                var random = new Random();


                Color myColor = Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));



                string hex = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");




                var color = String.Format("#{0:X6}", hex);

              
                Dictionarycolor.Add("ProjectId", p[0]);
                Dictionarycolor.Add("color", listhexColor[count]);





                ListOfcolors.Add(Dictionarycolor);

                count = (count + 1)% listhexColor.Count();
                


            }
                






            
            //  ListOfcolors[0]["ProjectId"]

            foreach (var date in ListOfDate)
            {
                foreach (var p in ListOfcolors)
                {
                    try
                    {
                        Dictionary<string, string> DictionaryOfEvents = new Dictionary<string, string>();

                        var events = GlobalIssues.Where(x => x["StartDate"] == date[0].ToString() && x["ProjectId"] == p["ProjectId"].ToString()).Distinct().ToList();

                       

                       


                        var eventsproject = events.GroupBy(x => x["ProjectName"])
                                           .Select(grp => grp.First())
                                           .Select(y => new[] { y["ProjectName"] })
                                        .ToList();


                    
                        DateTime oDate = Convert.ToDateTime(date[0].ToString());

                      //  var dt = oDate.Year + "-" + oDate.Month + "-" + oDate.Day;


                        var dt = oDate.Month + "/" + oDate.Day + "/" + oDate.Year;


                        


                        var eventscountByDate = events.Count();


                        if (eventscountByDate != 0)
                        {

                            DictionaryOfEvents.Add("title", eventscountByDate + " " + eventsproject[0][0].ToString());
                            DictionaryOfEvents.Add("start", ""+oDate);
                            DictionaryOfEvents.Add("color", p["color"].ToString());



                            ListOfEvents.Add(DictionaryOfEvents);

                        }

                    }
                    catch { }
                }

            }









            Dictionary.Add("data", ListOfEvents);

            Dictionary.Reverse();

            var jsonResult = JsonConvert.SerializeObject(Dictionary);



            return Json(jsonResult, JsonRequestBehavior.AllowGet);



        }




















        public Dictionary<string, int> getAllStatus()


        {
            List<int> PendingTasks = new List<int>();
            PendingTasks.Add(1);
            PendingTasks.Add(2);
            PendingTasks.Add(4);



            Dictionary<string, List<Models.Project>> issuesDictionary = new Dictionary<string, List<Models.Project>>();



            List<object> o = new List<object>();

            Dictionary<string, List<object>> Dictionary = new Dictionary<string, List<object>>();


            Dictionary<string, int> AllTicketDictionary = new Dictionary<string, int>();



            string username = "anthony.najour";
            string password = "123456";

            var url = FirstWebApp.Globals.Configuration.REDMINE;

            var manager = new RedmineManager(url, username, password);
            var user = manager.GetCurrentUser();

            var parameters = new NameValueCollection { { "status_id", "*" } };


            List<Dictionary<string, string>> ListDictionary = new List<Dictionary<string, string>>();

            foreach (var issue in manager.GetObjects<Issue>(parameters))
            {
                try
                {

                    Dictionary<string, string> issuesList = new Dictionary<string, string>();

                    issuesList.Add("Author", issue.Author == null ? "" : issue.Author.Name);

                    issuesList.Add("AssignedTo", issue.AssignedTo == null ? "" : issue.AssignedTo.Name);
                    issuesList.Add("Status", issue.Status == null ? "" : issue.Status.Name);
                    issuesList.Add("Subject", issue.Subject);
                    issuesList.Add("Description", issue.Description);
                    issuesList.Add("CreatedOn", issue.CreatedOn == null ? "" : issue.CreatedOn.Value.ToString());
                    issuesList.Add("Priority", issue.Priority == null ? "" : issue.Priority.Name);

                    issuesList.Add("ProjectId", issue.Project.Id == -1 ? "" : "" + issue.Project.Id);
                    issuesList.Add("ProjectName", issue.Project.Name == null ? "" : issue.Project.Name);

                    ListDictionary.Add(issuesList);

                }

                catch (Exception ex)
                {

                }

                }



                var alltickets = ListDictionary.Count();

            var newtickets = ListDictionary.Where(x => x["Status"] == "New").Count();


            var nbOfProject = ListDictionary.GroupBy(x => x["ProjectId"])
                  .Select(grp => grp.First())
              .ToList().Count();


            var nbOfEmployee = ListDictionary.GroupBy(x => x["AssignedTo"])
                 .Select(grp => grp.First())
             .ToList().Count();







            AllTicketDictionary.Add("alltickets", alltickets);



            AllTicketDictionary.Add("newtickets", newtickets);

            AllTicketDictionary.Add("nbOfProject", nbOfProject);

            AllTicketDictionary.Add("nbOfEmployee", nbOfEmployee);







            return AllTicketDictionary;



        }





    }
}