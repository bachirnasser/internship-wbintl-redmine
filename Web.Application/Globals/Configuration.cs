﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json;

using Web.Application.Controllers;

namespace FirstWebApp.Globals
{
    public static class Configuration
    {
        public static string APPLICATION_TITLE = "Modular";
        public static string APPLICATION_CLIENT = "Rolex";

        public static string APPLICATION_FILE_UPLOAD_STORAGE_PATH = @"C:\Temp";

        public static string REDMINE = "http://socrates.whitebeard.me:8082/redmine";

        public static string DATA_INTERFACE_URL = "http://localhost:57559/";
    }
}